//
//  ViewController.m
//  objc-demo
//
//  Created by Pablo Cosias on 26/7/17.
//  Copyright © 2017 eID. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController:UIViewController

    NSString *proxyEndpoint = @"https://etrust-sandbox.electronicid.eu/v2";
    NSString *apiEndpoint =  @"https://etrust-sandbox.electronicid.eu/v2";
    VideoViewController *videoVC;
    
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    videoVC = [[VideoViewController alloc] initWithListener:self];
    [self addChildViewController:videoVC];
    videoVC.view.frame = self.view.bounds;
    [self.view addSubview:videoVC.view];
    [videoVC didMoveToParentViewController:self];
    
    [videoVC initializeWithEidApi:apiEndpoint proxyEndpoint:proxyEndpoint];
    [videoVC turnOn];
    [videoVC startWithIdType:@"DOCUMENT-ID-TYPE"];}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void)videoIdFinishedWithVideoId:(NSString * _Nonnull)videoId
{


}
    
- (void)videoIdErrorWithError:(Error * _Nonnull)error
{


}


@end
