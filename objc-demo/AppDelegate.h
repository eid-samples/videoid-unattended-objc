//
//  AppDelegate.h
//  objc-demo
//
//  Created by Pablo Cosias on 26/7/17.
//  Copyright © 2017 eID. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

