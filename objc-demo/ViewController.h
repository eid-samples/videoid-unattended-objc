//
//  ViewController.h
//  objc-demo
//
//  Created by Pablo Cosias on 26/7/17.
//  Copyright © 2017 eID. All rights reserved.
//

#import <UIKit/UIKit.h>
@import eIDVideoID;

@interface ViewController : UIViewController<VideoIdListener>
    


@end

