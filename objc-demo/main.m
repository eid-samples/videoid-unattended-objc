//
//  main.m
//  objc-demo
//
//  Created by Pablo Cosias on 26/7/17.
//  Copyright © 2017 eID. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
